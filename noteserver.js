var express = require('express');
var sockjs = require('sockjs');
var http = require('http');
var redis = require('redis');

// Sockjs server
var sockjs_opts = {
    sockjs_url: "http://cdn.sockjs.org/sockjs-0.3.min.js"
};
var sockjs_serv = sockjs.createServer(sockjs_opts);
sockjs_serv.on('connection', function(conn) {
  var browser = redis.createClient();
  var browser_subscribe = false;
  browser.on("message", function(channel, message) {
    conn.write(message);
  });

  conn.on('data', function(message) {
    console.log('message:', message);
    try {
        message = JSON.parse(message);
        if (message.channel && !browser_subscribe) {
          pub_channel = message.channel;
          browser.subscribe('public', message.channel);
          browser_subscribe = true;
        } else if (!browser_subscribe) {
          browser.subscribe('public');
          browser_subscribe = true;
        }
    } catch (e) {
      message = message;
      browser.subscribe('public');
      browser_subscribe = true;
    }
  //publisher.publish(pub_channel, message);
  });
});
// Express server
var app = express();
var server = http.createServer(app);

sockjs_serv.installHandlers(server, {
    prefix: '/ws'
});
var port = (process.env.NODE_PORT || 11111);

console.log('Listening .....');
server.listen(port, '0.0.0.0');

app.get('/', function (req, res) {
    res.sendfile('./views/index.html');
});